# try-pages-with-sphinx

This is a repository for learning how I can make a pages site with the sphinx docs/build folder.

Seems pretty simple.

Follow 1 here https://codeberg.page/ that says:

```
Create a branch pages in a public repository:
git switch --orphan pages
git rm --cached -r .
```

The last command failed for me with an error. Probably just because it was a new repository.

Follow this to make a sphinx documentation project. I put it in the ./docs folder.
https://docs.codeberg.org/codeberg-pages/examples/docs-as-code/

Do sphinx-quickstart with separate build and source folders.

Add /docs/build/ to .gitignore

Build the documentation with
```sphinx-build -b html docs/source/ docs/build/```

git commit any changes to main branch. and push if you feel like it.

The build folder that was just made with sphinx-build is also in the pages branch. I copy the contents of the docs/build folder to the root so that is the index page for the pages site. and rm the docs folder because it is now useless. commit and push.
```
git switch pages
cp -r docs/build/* ./
rm -r docs/
git commit -am 'updated docs'
git push
```

