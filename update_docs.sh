#!/bin/bash

sphinx-build -b html docs/source/ docs/build/
git switch pages
cp -r docs/build/* ./
rm -r docs/
git commit -am 'updated docs'
git push
git switch main
