.. try-pages-with-sphinx documentation master file, created by
   sphinx-quickstart on Fri Jul 14 14:35:11 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to try-pages-with-sphinx's documentation!
=================================================

Hello! Again! More!

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   demo


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
